package hotciv.standard;

import hotciv.framework.Tile;

public class TileImpl implements Tile {
    private String tile;

    public TileImpl(String tileType) {
        this.tile = tileType;
    }

    @Override
    public String getTypeString() {
        return tile;
    }
}
