package hotciv.variants.WinnerStrategy;

import hotciv.framework.Game;
import hotciv.framework.Player;

public class AlphaWinningStrategy implements WinningStrategy {
    @Override
    public Player getWinner(Game game) {
        if (game.getAge() == -3000) {
            return Player.RED;
        } else {
            return null;
        }
    }
}
