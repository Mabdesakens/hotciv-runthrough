package hotciv.standard;

import hotciv.framework.City;
import hotciv.framework.Player;

public class CityImpl implements City {
    private Player player;
    private int treasury = 0;
    private String production;
    private String workForce;

    public CityImpl(Player player, int treasury, String production, String workForce) {
        this.player = player;
        this.treasury = treasury;
        this.production = production;
        this.workForce = workForce;
    }

    @Override
    public Player getOwner() {
        return this.player;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public int getTreasury() {
        return treasury;
    }

    @Override
    public void setTreasury(int amount) {
        treasury = treasury + amount;
    }

    @Override
    public String getProduction() {
        return production;
    }

    @Override
    public String getWorkforceFocus() {
        return workForce;
    }

    @Override
    public void setWorkforceFocus(String workforce) {
        workForce = workforce;
    }

    @Override
    public void setProduction(String productionType) {
        production = productionType;
    }
}