package hotciv.variants.WinnerStrategy;

import hotciv.framework.Game;
import hotciv.framework.Player;

public interface WinningStrategy {
    public Player getWinner(Game game);
}
