package hotciv.standard;

import hotciv.framework.*;


import hotciv.variants.AgingStrategy.BetaAgingStrategy;
import hotciv.variants.WinnerStrategy.BetaWinningStrategy;
import hotciv.variants.WorldStrategy.AlphaWorldStrategy;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class TestBetaCiv {
    private Game game;

    @Before
    public void setUp() {
        game = new GameImpl(new AlphaWorldStrategy(), new BetaWinningStrategy(), new BetaAgingStrategy());
    }

    @Test
    public void shouldBe100BCAfter39Turns() {
        for (int i = 0; i < 39; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(-100));
    }

    @Test
    public void shouldBe1BCAfter40Turns() {
        for (int i = 0; i < 40; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(-1));
    }

    @Test
    public void shouldBe1After41Turns() {
        for (int i = 0; i < 41; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1));
    }
}
