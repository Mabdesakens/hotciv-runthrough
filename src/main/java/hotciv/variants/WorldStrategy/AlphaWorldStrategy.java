package hotciv.variants.WorldStrategy;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.standard.CityImpl;
import hotciv.standard.TileImpl;
import hotciv.standard.UnitImpl;

import java.util.HashMap;
import java.util.Map;

public class AlphaWorldStrategy implements WorldStrategy {
    private Map<Position, CityImpl> city = new HashMap<>();
    private Map<Position, TileImpl> tile = new HashMap<>();
    private Map<Position, UnitImpl> unit = new HashMap<>();

    @Override
    public Map<Position, CityImpl> getCity() {
        city.put(new Position(1,1), new CityImpl(Player.RED, 0,GameConstants.ARCHER, GameConstants.foodFocus));
        city.put(new Position(4,1), new CityImpl(Player.BLUE, 0, GameConstants.LEGION, GameConstants.productionFocus));
        return city;
    }

    @Override
    public Map<Position, UnitImpl> getUnit() {
        unit.put(new Position(0,2), new UnitImpl(GameConstants.ARCHER, Player.RED));
        unit.put(new Position(2,0), new UnitImpl(GameConstants.LEGION, Player.RED));
        unit.put(new Position(3,1), new UnitImpl(GameConstants.ARCHER, Player.BLUE));
        unit.put(new Position(3,2), new UnitImpl(GameConstants.LEGION, Player.BLUE));
        unit.put(new Position(4,3), new UnitImpl(GameConstants.SETTLER, Player.RED));
        return unit;
    }

    @Override
    public Map<Position, TileImpl> getTile() {
        tile.put(new Position(1,0), new TileImpl(GameConstants.OCEANS));
        tile.put(new Position(0,1), new TileImpl(GameConstants.HILLS));
        tile.put(new Position(2,2), new TileImpl(GameConstants.MOUNTAINS));
        return tile;
    }
}
