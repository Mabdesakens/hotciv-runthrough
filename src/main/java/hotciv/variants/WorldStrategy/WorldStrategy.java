package hotciv.variants.WorldStrategy;

import hotciv.framework.Position;
import hotciv.standard.CityImpl;
import hotciv.standard.TileImpl;
import hotciv.standard.UnitImpl;

import java.util.Map;

public interface WorldStrategy {
    Map<Position, CityImpl> getCity();

    Map<Position, UnitImpl> getUnit();

    Map<Position, TileImpl> getTile();
}
