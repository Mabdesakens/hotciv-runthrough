package hotciv.variants.AgingStrategy;

import hotciv.framework.Game;

public interface AgingStrategy {
    public int calculateAge(int age);
}
