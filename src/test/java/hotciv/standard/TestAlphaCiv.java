package hotciv.standard;

import hotciv.framework.*;

import hotciv.variants.AgingStrategy.AlphaAgingStrategy;
import hotciv.variants.WinnerStrategy.AlphaWinningStrategy;
import hotciv.variants.WinnerStrategy.WinningStrategy;
import hotciv.variants.WorldStrategy.AlphaWorldStrategy;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/** Skeleton class for AlphaCiv test cases

    Updated Oct 2015 for using Hamcrest matchers

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 $
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
public class TestAlphaCiv {
  private Game game;

  /** Fixture for alphaciv testing. */
  @Before
  public void setUp() {
    game = new GameImpl(new AlphaWorldStrategy(), new AlphaWinningStrategy(), new AlphaAgingStrategy());
  }

  // FRS p. 455 states that 'Red is the first player to take a turn'.
  @Test
  public void shouldBeRedAsStartingPlayer() {
    assertThat(game, is(notNullValue()));
    assertThat(game.getPlayerInTurn(), is(Player.RED));
  }

  @Test
  public void blueShouldBeSecondInPlay() {
    game.endOfTurn();
    assertThat(game.getPlayerInTurn(), is(Player.BLUE));
  }

  @Test
  public void shouldBeRedInTurn3() {
    game.endOfTurn();
    game.endOfTurn();
    assertThat(game.getPlayerInTurn(), is(Player.RED));
  }

  @Test
  public void redCityShouldBeOn11() {
    Position redCity = new Position(1,1);
    assertThat(game.getCityAt(redCity).getOwner(), is(Player.RED));
  }

  @Test
  public void shouldBeBlueCityOn41() {
    Position blueCity = new Position(4,1);
    assertThat(game.getCityAt(blueCity).getOwner(), is(Player.BLUE));
  }

  @Test
  public void shouldBOceanAt10() {
    Position Ocean = new Position(1,0);
    assertThat(game.getTileAt(Ocean).getTypeString(), is(GameConstants.OCEANS));
  }

  @Test
  public void shouldBeHillsAt01() {
    Position Hills = new Position(0,1);
    assertThat(game.getTileAt(Hills).getTypeString(), is(GameConstants.HILLS));
  }

  @Test
  public void shouldBeMountsAt22() {
    Position Mountains = new Position(2,2);
    assertThat(game.getTileAt(Mountains).getTypeString(), is(GameConstants.MOUNTAINS));
  }

  @Test
  public void shouldBeArcherAt20IsRed() {
    Position RedArcher = new Position(2,0);
    assertThat(game.getUnitAt(RedArcher).getOwner(), is(Player.RED));
  }

  @Test
  public void shouldBeUnitAt32IsBlue() {
    Position BlueLegion = new Position(3,2);
    assertThat(game.getUnitAt(BlueLegion).getOwner(), is(Player.BLUE));
  }

  @Test
  public void shouldBeUnitAt20IsArcher() {
    Position RedLegion = new Position(2,0);
    assertThat(game.getUnitAt(RedLegion).getTypeString(), is(GameConstants.LEGION));
  }

  @Test
  public void shouldBeUnitAt32IsLegion() {
    Position RedArcher = new Position(3,2);
    assertThat(game.getUnitAt(RedArcher).getTypeString(), is(GameConstants.LEGION));
  }

  @Test
  public void shouldBeYear4000BCWhenStarting() {
    assertThat(game.getAge(), is(-4000));
  }

  @Test
  public void shouldBeYear2800BCAfterTwoTurns() {
    game.endOfTurn();
    assertThat(game.getAge(), is(-3900));
  }

  @Test
  public void shouldBeRedWinsInYear3000() {
    for (int i = 0; i<10; i++) {
      game.endOfTurn();
    }
    assertThat(game.getWinner(), is(Player.RED));
  }

  @Test
  public void shouldBeRedIsNotWinningInYear3500() {
    for (int i = 0; i<5; i++) {
      game.endOfTurn();
    }
    assertThat(game.getWinner(), is(nullValue()));
  }

  @Test
  public void shouldBePlainsAt88() {
    Position Plains = new Position(8,8);
    assertThat(game.getTileAt(Plains).getTypeString(), is(GameConstants.PLAINS));
  }

  @Test
  public void shouldBeRedArcherTo20IsTrue() {
    Position from = new Position(2,0);
    Position to = new Position(2,1);
    assertThat(game.moveUnit(from,to), is(true));
  }

  @Test
  public void shouldBeRedArcherTo22IsFalse() {
    Position from = new Position(2,0);
    Position to = new Position(2,2);
    assertThat(game.moveUnit(from,to), is(false));
  }

  @Test
  public void shouldBeBlueLegionTo33IsTrue() {
    Position from = new Position(3,2);
    Position to = new Position(3,3);
    assertThat(game.moveUnit(from,to), is(true));
  }

  @Test
  public void shouldBeLegionMovingToMountainIsFalse() {
    Position from = new Position(3,2);
    Position to = new Position(2,2);
    assertThat(game.moveUnit(from,to), is(false));
  }

  @Test
  public void shouldBeRedArcherMovingToOceanIsFalse() {
    Position from = new Position(2,0);
    Position to = new Position(1,0);
    assertThat(game.moveUnit(from,to), is(false));
  }

  @Test
  public void shouldBeLegalToMoveToHills() {
    Position from = new Position(0,2);
    assertThat(game.moveUnit(from, new Position(0,1)), is(true));
  }

  @Test
  public void doesBlueLegionDisappearWhenMovedOnto() {
    Position from = new Position(2,0);
    Position to = new Position(3,1);
    game.moveUnit(from, to);
    assertThat(game.getUnitAt(to).getOwner(), is(Player.RED));
  }

  @Test
  public void redCityShouldHave6ProductionAfter1Round() {
    game.endOfTurn();
    game.endOfTurn();
    assertThat(game.getCityAt(new Position(1,1)).getTreasury(), is(6));
  }

  @Test
  public void blueCityShouldHave12ProductionAfter2Rounds() {
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    assertThat(game.getCityAt(new Position(4,1)).getTreasury(), is(12));
  }

  @Test
  public void redCityShouldBeProducingArchers() {
    assertThat(game.getCityAt(new Position(1,1)).getProduction(), is(GameConstants.ARCHER));
  }

  @Test
  public void blueCityShouldBeProducingLegions() {
    assertThat(game.getCityAt(new Position(4, 1)).getProduction(), is(GameConstants.LEGION));
  }

  @Test
  public void redCityShouldHaveSize1() {
    assertThat(game.getCityAt(new Position(1,1)).getSize(), is(1));
  }

  @Test
  public void redCityWorkForceShouldBeApplesToStartWith() {
    assertThat(game.getCityAt(new Position(1,1)).getWorkforceFocus(), is(GameConstants.foodFocus));
  }

  @Test
  public void blueCityWorkForceShouldBeApplesAfterChange() {
    game.changeWorkForceFocusInCityAt(new Position(4,1),GameConstants.foodFocus);
    assertThat(game.getCityAt(new Position(4,1)).getWorkforceFocus(), is(GameConstants.foodFocus));
  }

  @Test
  public void changeProductionFunctionWorks() {
    Position redCity = new Position(1,1);
    game.changeProductionInCityAt(redCity, GameConstants.LEGION);
    assertThat(game.getCityAt(redCity).getProduction(), is(GameConstants.LEGION));
  }

  @Test
  public void redCityEarnsMoneyEveryRound() {
    Position redCity = new Position(1,1);
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    assertThat(game.getCityAt(redCity).getTreasury(), is(12));
  }

  @Test
  public void makeUnitMethodShouldSpawnArcherAt66ThatIsRed() {
    Position unitPosition = new Position(1,1);
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.makeUnit(unitPosition);
    assertThat(game.getUnitAt(unitPosition).getTypeString(), is(GameConstants.ARCHER));
    assertThat(game.getUnitAt(unitPosition).getOwner(), is(Player.RED));
  }

  @Test
  public void makeUnitMakesUnitInProduction() {
    Position blueCity = new Position(4,1);
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.makeUnit(blueCity);
    assertThat(game.getUnitAt(blueCity).getTypeString(), is(GameConstants.LEGION));
  }

  @Test
  public void whenMakingAUnitCityTreasuryIsDecreased() {
    Position redCity = new Position(1,1);
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.makeUnit(redCity);
    assertThat(game.getCityAt(redCity).getTreasury(), is(2));
  }

  @Test
  public void redCityShouldNotBeAbleToMakeArcherInFirstTurn() {
    Position redCity = new Position(1,1);
    game.makeUnit(redCity);
  }



}
