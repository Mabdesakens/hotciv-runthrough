package hotciv.variants.AgingStrategy;

import hotciv.framework.Game;

public class AlphaAgingStrategy implements AgingStrategy {

    @Override
    public int calculateAge(int age) {
        return age + 100;
    }
}
