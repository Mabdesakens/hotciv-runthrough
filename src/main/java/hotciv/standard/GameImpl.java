package hotciv.standard;

import hotciv.framework.*;
import hotciv.variants.AgingStrategy.AgingStrategy;
import hotciv.variants.AgingStrategy.AlphaAgingStrategy;
import hotciv.variants.WinnerStrategy.WinningStrategy;
import hotciv.variants.WorldStrategy.WorldStrategy;

import java.util.HashMap;
import java.util.Map;

public class GameImpl implements Game {
    private Map<Position, CityImpl> cityMap = new HashMap<>();
    private Map<Position, TileImpl> tileMap = new HashMap<>();
    private Map<Position, UnitImpl> unitMap = new HashMap<>();
    private Player playerInTurn = Player.RED;
    private int timeConstant = -4000;

    private AgingStrategy agingStrategy;
    private WorldStrategy worldStrategy;
    private WinningStrategy winningStrategy;


    public GameImpl(WorldStrategy worldStrategy, WinningStrategy winningStrategy, AgingStrategy agingStrategy) {
        cityMap = worldStrategy.getCity();
        tileMap = worldStrategy.getTile();
        unitMap = worldStrategy.getUnit();

        this.worldStrategy = worldStrategy;
        this.agingStrategy = agingStrategy;
        this.winningStrategy = winningStrategy;
    }

    public Tile getTileAt( Position p ) {
        if (tileMap.get(p) == null ) {
            return new TileImpl(GameConstants.PLAINS);
        }
        return tileMap.get(p);
    }
    public Unit getUnitAt( Position p ) { return unitMap.get(p); }
    public City getCityAt( Position p ) { return cityMap.get(p); }
    public Player getPlayerInTurn() { return playerInTurn; }
    public Player getWinner() {
        return winningStrategy.getWinner(this);
    }

    public int getAge() { return timeConstant; }

    public boolean moveUnit( Position from, Position to ) {
        int defRow = from.getRow() - to.getRow();
        int defColumn = from.getColumn() - to.getColumn();
        UnitImpl mover = (UnitImpl) getUnitAt(from);

        if (defRow <= 1 && defRow >= -1 && defColumn <= 1 && defColumn >= -1) {

            //returns false if moveTo position is other than Plains and Hills
            if (tileMap.get(to) != null && tileMap.get(to).getTypeString() != GameConstants.HILLS) {
                return false;
            }
            //returns true is qualified to move to the given position
            unitMap.remove(from);
            unitMap.put(to, mover);
            return true;
        }
        return false;
    }
    public void endOfTurn() {
        if (playerInTurn == Player.RED) {
            playerInTurn = Player.BLUE;
            timeConstant = agingStrategy.calculateAge(timeConstant);
        } else {
            playerInTurn = Player.RED;
            timeConstant = agingStrategy.calculateAge(timeConstant);

            cityMap.put(new Position(1,1),(CityImpl) getCityAt(new Position(1,1))).setTreasury(6);
            cityMap.put(new Position(4,1),(CityImpl) getCityAt(new Position(4,1))).setTreasury(6);
        }
    }
    public void changeWorkForceFocusInCityAt( Position p, String balance ) {
        cityMap.put(p, (CityImpl) getCityAt(p)).setWorkforceFocus(balance);
    }
    public void changeProductionInCityAt( Position p, String unitType ) {
        cityMap.put(p, (CityImpl) getCityAt(p)).setProduction(unitType);
    }
    public void performUnitActionAt( Position p ) {}

    @Override
    public void makeUnit( Position p ) {
        if (getCityAt(p).getProduction() == GameConstants.ARCHER && getCityAt(p).getTreasury() >= 10) {
            unitMap.put(p, (UnitImpl) new UnitImpl(GameConstants.ARCHER, playerInTurn));
            cityMap.put(p, ((CityImpl) cityMap.get(p))).setTreasury(-10);
        }

        if (getCityAt(p).getProduction() == GameConstants.LEGION && getCityAt(p).getTreasury() >= 15) {
            unitMap.put(p, (UnitImpl) new UnitImpl(GameConstants.LEGION, playerInTurn));
            cityMap.put(p, ((CityImpl) cityMap.get(p))).setTreasury(-15);
        }

        if (getCityAt(p).getProduction() == GameConstants.SETTLER && getCityAt(p).getTreasury() >= 30) {
            unitMap.put(p, (UnitImpl) new UnitImpl(GameConstants.SETTLER, playerInTurn));
            cityMap.put(p, ((CityImpl) cityMap.get(p))).setTreasury(-30);
        }
    }
}
